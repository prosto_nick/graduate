package serial;

public class Node {

    private final int column;
    private final float value;
    private Node leftChild;
    private Node rightChild;
    private float leftLeaf=-1;
    private float rightLeaf=-1;

    public float getLeftLeaf() {
        return leftLeaf;
    }


    public float getRightLeaf() { return rightLeaf;
    }

    public void setLeftLeaf(float leaf) {
        this.leftLeaf = leaf;
    }

    public void setRightLeaf(float leaf) {this.rightLeaf = leaf;
    }

    Node(int column, float value){
        this.column = column;
        this.value = value;
    }


    public int getColumn() {
        return column;
    }

    public float getValue() {
        return value;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public String toString() {

        String result =  "[x" + column + " < " + value + "]";

        if (leftLeaf + rightLeaf == -2f) {
           return result;
        }

        if (leftLeaf == -1f && rightLeaf != -1){
            return result + " - "  + "[right = " + rightLeaf + "]";
        }

        if (rightLeaf == -1f && leftLeaf != -1){
            return result + " - "  + "[left = " + leftLeaf + "]";
        }

        return result + " - "  + "[left = " + leftLeaf + "]" + "[right = " + rightLeaf + "]";

    }


}
