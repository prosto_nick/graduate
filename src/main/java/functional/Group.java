package functional;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private final List<List<Float>> group;

    public Group(){ this.group = new ArrayList<>();}

    public Group(List<List<Float>> group) {
        this.group = group;
    }

    public List<List<Float>> getGroup() {
        return group;
    }

    public List<Float> getRow(int i) {
        return group.get(i);
    }

    public int getSize(){
        return group.size();
    }

    public boolean isNotEmpty() {
        return (!group.isEmpty());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group1 = (Group) o;

        return group != null ? group.equals(group1.group) : group1.group == null;
    }

    @Override
    public int hashCode() {
        return group != null ? group.hashCode() : 0;
    }


}
