package functional;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TreeBuilder {

    final int LEFT = 0;
    final int RIGHT = 1;


    /**
     *<p>This function counts how much rows of certain class are contained in group </p>
     * @param classNumber is a value of certain class
     * @param group is a loads of rows which consists of list's float
     * @return amount of rows which belongs given classNumber
     */
    int countClassInGroup(final float classNumber, final Group group) {
       return (int) group.getGroup().stream()
                .filter(row -> row.get(row.size()-1).equals(classNumber))
                .count();
    }

    /**
     * <p>This function counts the amount of rows in all groups</p>
     * @param groups is a list which contains several groups
     * @return amount all rows in all groups
     */
    int countAllRows (final List<Group> groups) {

        return  groups.stream()
                .mapToInt(group -> group.getSize())
                .sum();
    }

    /**
     * <p>This function counts Ginni scores in group</p>
     * @param group is a loads of rows which consists of list's float
     * @param classes is a list which contains values of classes
     * @return sum of Ginni scores for certain group
     */
    private float countScoresInGroup (final Group group, final List<Float> classes){

        return (float)classes.stream()
                .mapToDouble(crtClass -> (float) countClassInGroup(crtClass, group) / group.getSize())
                .map(proportion -> proportion*proportion)
                .sum();

    }

    /**
     * <p>This function counts Ginni index</p>
     * @param classes is a list which contains values of classes
     * @param groups is a list which contains several groups
     * @return Ginni index
     */
    float getGinni (List<Float> classes, List<Group> groups) {

        return (float) groups.stream()
                .filter(group -> group.isNotEmpty())
                .mapToDouble(group -> (1 - countScoresInGroup(group, classes)) * group.getSize() / countAllRows(groups))
                .sum();
    }

    /**
     * <p>This function splits a dataset into two groups
     *    given the index of an attribute and a split value for that attribute.</p>
     * @param column is a number of column with attribute by which we want to split
     * @param columnValue is an attribute value by which we want to split
     * @param dataset is the data that is used to build a decision tree
     * @return list of divided groups
     */
    List<Group> splitOnGroups (final int column, final float columnValue, final Group dataset){

         Map<Boolean, List<List<Float>>> crt = dataset.getGroup().stream()
                .collect(Collectors.partitioningBy(row -> row.get(column) < columnValue));

         return List.of(new Group(crt.get(true)), new Group(crt.get(false)));
    }

    /**
     * <p>This function counts all classes </p>
     * @param dataset is the data that is used to build a decision tree
     * @return a list of classes values
     */
     List<Float> countClasses (final Group dataset) {
        return dataset.getGroup().stream()
                .map(row -> row.get(row.size()-1))
                //убираем дубликаты
                .distinct()
                .collect(Collectors.toUnmodifiableList());
    }

    /**
     * <p>This function counts how often each class is found in a group and selects the most popular one</p>
     * @param group is a loads of rows which consists of list's float
     * @return the most common class value
     */
    float findBestClassInGroup (final Group group) {

        return countClasses(group).stream()
                .flatMap(classNumb -> Map.of(countClassInGroup(classNumb, group), classNumb).entrySet().stream())
                .max((m1, m2) -> Float.compare(m1.getKey(), m2.getKey()))
                .get().getValue();
    }


    /**
     * <p>This function finds the best partition for the data row</p>
     * @param row is a list of attributes where last attribute is a class value
     * @param dataset is the data that is used to build a decision tree
     * @return immutable map with a minimum value of Ginni index in row,
     *        which contains the attribute, its value, list of groups and Ginni index
     */
    Map<String, Object> bestSplitForRow (final List<Float> row, final Group dataset) {

        return  IntStream.range(0, row.size() - 1).parallel()
                .mapToObj(i -> Map.of("column", i,
                        "columnValue", row.get(i),
                        "groups", splitOnGroups(i, row.get(i), dataset),
                        "ginni", getGinni(countClasses(dataset), splitOnGroups(i, row.get(i), dataset))))
                .min((map1, map2) -> Float.compare((float) map1.get("ginni"), (float) map2.get("ginni")))
                .get();
    }

    /**
     *<p>This function finds the best partition for dataset</p>
     * @param dataset is the data that is used to build a decision tree
     * @return immutable map with a minimum value of Ginni index in dataset
     *        which contains the attribute, its value, list of groups and Ginni index
     */
    Map<String, Object> bestSplit (final Group dataset){

        return dataset.getGroup().parallelStream()
                .map((row) -> bestSplitForRow(row, dataset))
                .min((map1, map2) -> Float.compare((float) map1.get("ginni"), (float) map2.get("ginni")))
                .get();
    }



    /**
     *<p> This function calls function for best split, checks received groups of the limitations
     *    on depth and size and creates the leaves or called recursively</p>
     * @param maxDepth is a maximum depth of a decision tree
     * @param minSizeOfGroup is a minimum size of a group to continue split
     * @param crtDepth is a depth of a current level
     * @param group is a loads of rows which consists of list's float
     * @return immutable map which contains the attribute, its value and links to child nodes
     */
    Map<String, Object> split (final int maxDepth, final int minSizeOfGroup, final int crtDepth, final Group group) {

        final Map<String, Object> crtSplit = bestSplit(group);
        final Group leftGroup = ((List<Group>) crtSplit.get("groups")).get(LEFT);
        final Group rightGroup = ((List<Group>) crtSplit.get("groups")).get(RIGHT);


        if (leftGroup.isNotEmpty() && !rightGroup.isNotEmpty()) {
                return Map.of("answer", findBestClassInGroup(leftGroup));
        } else if (rightGroup.isNotEmpty() && !leftGroup.isNotEmpty()) {
                return Map.of("answer", findBestClassInGroup(rightGroup));
        }

//        if (crtDepth >= maxDepth){
//
//            return Map.of(
//                    "column", crtSplit.get("column"),
//                    "columnValue", crtSplit.get("columnValue"),
//                    "leftChild", Map.of("answer", findBestClassInGroup(leftGroup)),
//                    "rightChild", Map.of("answer", findBestClassInGroup(rightGroup)));
//        }

        return Map.of(
                "column", crtSplit.get("column"),
                "columnValue", crtSplit.get("columnValue"),
                "leftChild", (crtDepth >= maxDepth || leftGroup.getSize() <= minSizeOfGroup) ?
                            Map.of("answer", findBestClassInGroup(leftGroup)) :
                            split(maxDepth, minSizeOfGroup, crtDepth+1, leftGroup),
                "rightChild", (crtDepth >= maxDepth || rightGroup.getSize() <= minSizeOfGroup) ?
                            Map.of("answer", findBestClassInGroup(rightGroup)) :
                            split(maxDepth, minSizeOfGroup, crtDepth+1, rightGroup));
    }

    /**
     * <p>This function builds a decision tree</p>
     * @param trainData is the data that is used to build a decision tree
     * @param maxDepth is a maximum depth of a decision tree
     * @param minSizeOfGroup is a minimum size of a group to continue split
     * @return immutable map which contains a root of a decision tree
     */
    Map<String, Object> buildTree (Group trainData, int maxDepth, int minSizeOfGroup) {

        if (trainData.isNotEmpty()) {
            return split(maxDepth, minSizeOfGroup, 1, trainData);
        } else {
            throw new IllegalArgumentException("Error! Dataset is empty!");
        }
    }

    void printTree (final Map<String, Object> node, String shift){

        if (node != null) {

            if (node.containsKey("answer")) {
                System.out.println(shift + "[class = " + node.get("answer") + "]");
            } else {
                System.out.println(shift + "x[" + node.get("column") + "] < " + node.get("columnValue"));
                printTree( (Map<String,Object>) node.get("leftChild"), shift + "   ");
                printTree( (Map<String,Object>) node.get("rightChild"), shift + "   ");
            }
        } else {
            System.out.println(shift + "*");
        }
    }

    /**
     * <p>This function make an answer prediction for each row of the dataset</p>
     * @param node is an immutable map of a current node
     * @param dataset is a data for which the prediction is made
     * @return an immutable list which contains answer predictions for all rows of the dataset
     */
    List<Float> makePredictionForDataset (final Map<String, Object> node, final Group dataset){

        return  dataset.getGroup().stream()
                .map(row -> makePredictionForRow(node, row))
                .collect(Collectors.toUnmodifiableList());
    }

    /**
     * <p>This function make an answer prediction for a row</p>
     * @param node is an immutable map of a current node
     * @param row is an immutable list of attributes
     * @return prediction which contains class value (answer)
     */
    float makePredictionForRow(Map<String, Object> node, List<Float> row) {

        if (node.containsKey("answer")) {
            return (float) node.get("answer");
        }

        if (row.get( (int) node.get("column")) < (float) node.get("columnValue")) {
            return makePredictionForRow( (Map<String, Object>) node.get("leftChild"), row);
        } else {
            return makePredictionForRow( (Map<String, Object>) node.get("rightChild"), row);
        }
    }

}
