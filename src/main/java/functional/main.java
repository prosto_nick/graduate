package functional;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class main {

    public static void main(String[] args) throws IOException {

        BufferedReader csvTrain = new BufferedReader(new FileReader("/home/gravity_falls/IdeaProjects/graduate20/src/main/resources/trainData.csv"));
        BufferedReader csvReal = new BufferedReader(new FileReader("/home/gravity_falls/IdeaProjects/graduate20/src/main/resources/realData.csv"));

        csvTrain.readLine();
        Group trainData = new Group(
                 csvTrain.lines()
                .map(str -> Arrays.stream(str.split(","))
                        .map(elem -> Float.parseFloat(elem))
                        .collect(Collectors.toUnmodifiableList()))
                .collect(Collectors.toUnmodifiableList()));

        csvTrain.close();

        FileWriter writer = new FileWriter("results.txt", true);
        for(int i=0; i<500; i++) {
            long start = System.currentTimeMillis();
            TreeBuilder builder = new TreeBuilder();

            Map<String, Object> root = builder.buildTree(trainData, 80, 1);
            long finish = System.currentTimeMillis();
            long workTime = finish - start;
            String str = Long.toString(workTime);
            writer.write(str);
            writer.append('\n');
        }
        writer.close();
//        builder.printTree(root, "");


        csvReal.readLine();
        Group realData = new Group(csvReal.lines()
                .map(str -> Arrays.stream(str.split(","))
                      .map(elem -> Float.parseFloat(elem))
                      .collect(Collectors.toUnmodifiableList()))
                .collect(Collectors.toUnmodifiableList()));

        csvReal.close();


//        List<Float> predictions = builder.makePredictionForDataset(root, realData);
//        for (int i = 0; i < predictions.size(); i++) {
//            System.out.println( i+1 + " - " + predictions.get(i));
//        }



    }
}
