package functional;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TreeBuilderTest {

    @Test
    void GinniTest1() {
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                Arrays.asList(1f, 1f),
                Arrays.asList(2f, 1f))
        );

        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(0f, 0f),
                Arrays.asList(1f, 0f))
        );

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        functional.TreeBuilder builder = new TreeBuilder();

        assertEquals(0f, builder.getGinni(classes, groups));
    }

    @Test
    void GinniTest2() {
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                Arrays.asList(1f, 0.3f, 1f, 0f),
                Arrays.asList(2f, 0f, 1f, 0f),
                Arrays.asList(3f, 0f, 1f, 1f),
                Arrays.asList(2f, 0f, 1f, 1f))
        );

         Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(5f, 0.1f, 0f, 1f),
                Arrays.asList(1f, 0f, 2f, 0f),
                Arrays.asList(2f, 0.5f, 1f, 1f),
                Arrays.asList(3f, 0.2f, 1f, 0f))
        );

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        functional.TreeBuilder builder = new TreeBuilder();

        assertEquals(0.5f, builder.getGinni(classes, groups));
    }

    @Test
    void GinniTest3(){
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group();

        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(0f, 0f),
                Arrays.asList(0f, 1f),
                Arrays.asList(1f, 0f),
                Arrays.asList(2f, 0f))
        );

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.375f, builder.getGinni(classes, groups));
    }

    @Test
    void GinniTest4(){
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                        Arrays.asList(5f, 2f, 1f),
                        Arrays.asList(0f, 0f, 1f),
                        Arrays.asList(1f, 1f, 1f),
                        Arrays.asList(0f, 1f, 1f),
                        Arrays.asList(1f, 0f, 1f),
                        Arrays.asList(0f, 0f, 1f),
                        Arrays.asList(2f, 0f, 0f),
                        Arrays.asList(2f, 1f, 0f))
        );

        Group secondGroup = new Group(Arrays.asList(
                        Arrays.asList(0f, 0f, 0f),
                        Arrays.asList(0f, 1f, 1f),
                        Arrays.asList(1f, 0f, 0f),
                        Arrays.asList(1f, 1f, 1f))
        );

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.4166667f, builder.getGinni(classes, groups));
    }

    @Test
    void GinniTest5(){
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                        Arrays.asList(0.5f, 1.2f, 0f),
                        Arrays.asList(0.3f, 0.8f, 0f),
                        Arrays.asList(0.7f, 1.1f, 1f))
        );


        Group secondGroup = new Group(Arrays.asList(
                        Arrays.asList(1.0f, 0.5f, 0f),
                        Arrays.asList(0.9f, 1.3f, 1f))
        );

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.46666664f, builder.getGinni(classes, groups));

    }

    @Test
    void GinniTest6() {
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                        Arrays.asList(0.5f, 1.2f, 0f),
                        Arrays.asList(0.3f, 0.8f, 0f))
        );

        Group secondGroup = new Group(Arrays.asList(
                        Arrays.asList(0.7f, 1.1f, 1f),
                        Arrays.asList(1.0f, 0.5f, 0f),
                        Arrays.asList(0.9f, 1.3f, 1f))
        );


        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.26666665f, builder.getGinni(classes, groups));
    }

    @Test
    void GinniTest7() {
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                Arrays.asList(1.1f, 2.4f, 0f),
                Arrays.asList(1.6f, 2.8f, 0f),
                Arrays.asList(0.9f, 3.5f, 0f),
                Arrays.asList(1.8f, 1.5f, 0f),
                Arrays.asList(1.9f, 2.7f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(3.5f, 2.2f, 1f),
                Arrays.asList(4.1f, 3.3f, 1f),
                Arrays.asList(3.7f, 2.9f, 1f)));

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0f, builder.getGinni(classes, groups));
    }

    @Test
    void GinniTest8(){
        List<Float> classes = Arrays.asList(0f, 1f);

        Group firstGroup = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(2f, 1f, 1f),
                Arrays.asList(2f, 3f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(3f, 2f, 0f)));

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.26666665f, builder.getGinni(classes, groups));
    }

    @Test
    void TestSplit1(){

        Group dataset = new Group(Arrays.asList(
                    Arrays.asList(1.1f, 2.4f, 0f),
                    Arrays.asList(3.5f, 2.2f, 1f),
                    Arrays.asList(1.6f, 2.8f, 0f),
                    Arrays.asList(0.9f, 3.5f, 0f),
                    Arrays.asList(1.8f, 1.5f, 0f),
                    Arrays.asList(4.1f, 3.3f, 1f),
                    Arrays.asList(3.7f, 2.9f, 1f),
                    Arrays.asList(1.9f, 2.7f, 0f)));

        Group firstGroup = new Group(Arrays.asList(
                    Arrays.asList(1.1f, 2.4f, 0f),
                    Arrays.asList(1.6f, 2.8f, 0f),
                    Arrays.asList(0.9f, 3.5f, 0f),
                    Arrays.asList(1.8f, 1.5f, 0f),
                    Arrays.asList(1.9f, 2.7f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                    Arrays.asList(3.5f, 2.2f, 1f),
                    Arrays.asList(4.1f, 3.3f, 1f),
                    Arrays.asList(3.7f, 2.9f, 1f)));

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        final int column = 0;
        final float value = 3.5f;

        functional.TreeBuilder builder = new TreeBuilder();

        assertEquals(groups,  builder.splitOnGroups(column, value, dataset));

    }

   @Test
    void TestOnBestSplit1 (){
        Group dataset = new Group(Arrays.asList(
                    Arrays.asList(1.1f, 2.4f, 0f),
                    Arrays.asList(3.5f, 2.2f, 1f),
                    Arrays.asList(1.6f, 2.8f, 0f),
                    Arrays.asList(0.9f, 3.5f, 0f),
                    Arrays.asList(1.8f, 1.5f, 0f),
                    Arrays.asList(4.1f, 3.3f, 1f),
                    Arrays.asList(3.7f, 2.9f, 1f),
                    Arrays.asList(1.9f, 2.7f, 0f)));

        Group firstGroup = new Group(Arrays.asList(
                    Arrays.asList(1.1f, 2.4f, 0f),
                    Arrays.asList(1.6f, 2.8f, 0f),
                    Arrays.asList(0.9f, 3.5f, 0f),
                    Arrays.asList(1.8f, 1.5f, 0f),
                    Arrays.asList(1.9f, 2.7f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                    Arrays.asList(3.5f, 2.2f, 1f),
                    Arrays.asList(4.1f, 3.3f, 1f),
                    Arrays.asList(3.7f, 2.9f, 1f)));

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);
        final int bestColumn = 0;
        final float bestValue = 3.5f;
        final float Ginni =0;

        Map<String, Object> result = Map.of(
                "column", bestColumn,
                "columnValue", bestValue,
                "groups", groups,
                "ginni", Ginni);

        functional.TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void TestOnBestSplit2 (){
        Group dataset = new Group(Arrays.asList(
                    Arrays.asList(0.5f, 1.2f, 0f),
                    Arrays.asList(0.3f, 0.8f, 0f),
                    Arrays.asList(0.7f, 1.1f, 1f),
                    Arrays.asList(1.0f, 0.5f, 0f),
                    Arrays.asList(0.9f, 1.3f, 1f)));


        Group firstGroup = new Group(Arrays.asList(
                    Arrays.asList(0.5f, 1.2f, 0f),
                    Arrays.asList(0.3f, 0.8f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                    Arrays.asList(0.7f, 1.1f, 1f),
                    Arrays.asList(1.0f, 0.5f, 0f),
                    Arrays.asList(0.9f, 1.3f, 1f)));

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        final int bestColumn = 0;
        final float bestValue = 0.7f;
        final float Ginni = 0.26666665f;

        Map<String, Object> result = Map.of(
                "column", bestColumn,
                "columnValue", bestValue,
                "groups", groups,
                "ginni", Ginni);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void TestOnBestSplit3() {

        Group dataset = new Group(Arrays.asList(
                    Arrays.asList(1f, 4f, 1f),
                    Arrays.asList(3f, 7f, 0f),
                    Arrays.asList(2f, 1f, 1f),
                    Arrays.asList(3f, 2f, 0f),
                    Arrays.asList(2f, 3f, 0f)));

        Group firstGroup = new Group(Arrays.asList(
                    Arrays.asList(1f, 4f, 1f),
                    Arrays.asList(2f, 1f, 1f),
                    Arrays.asList(2f, 3f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                    Arrays.asList(3f, 7f, 0f),
                    Arrays.asList(3f, 2f, 0f)));


        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        final int bestColumn = 0;
        final float bestValue = 3f;
        final float Ginni = 0.26666665f;

        Map<String, Object> result = Map.of(
                "column", bestColumn,
                "columnValue", bestValue,
                "groups", groups,
                "ginni", Ginni);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void TestOnBestSplit4() {

        Group dataset = new Group(Arrays.asList(
                        Arrays.asList(1.1f, 2.4f, 0f),
                        Arrays.asList(1.6f, 2.8f, 0f),
                        Arrays.asList(0.9f, 3.5f, 0f),
                        Arrays.asList(1.8f, 1.5f, 0f),
                        Arrays.asList(1.9f, 2.7f, 0f)));

        Group firstGroup = new Group(Arrays.asList(
                Arrays.asList(0.9f, 3.5f, 0f)));


        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(1.1f, 2.4f, 0f),
                Arrays.asList(1.6f, 2.8f, 0f),
                Arrays.asList(1.8f, 1.5f, 0f),
                Arrays.asList(1.9f, 2.7f, 0f)));

        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        final int bestColumn = 0;
        final float bestValue = 1.1f;
        final float Ginni = 0f;

        Map<String, Object> result = Map.of(
                "column", bestColumn,
                "columnValue", bestValue,
                "groups", groups,
                "ginni", Ginni);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void countAllRowsTest1(){

        Group firstGroup = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(2f, 1f, 1f),
                Arrays.asList(2f, 3f, 0f)));

        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(3f, 2f, 0f)));


        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();
        int result = 5;

        assertEquals(result,  builder.countAllRows(groups));
    }

    @Test
    void countAllRowsTest2(){

        Group firstGroup = new Group();

        Group secondGroup = new Group(Arrays.asList(
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(3f, 2f, 0f)));


        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();
        int result = 2;

        assertEquals(result,  builder.countAllRows(groups));
    }

    @Test
    void countClassInGroupTest1() {
        Group group = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(2f, 1f, 1f),
                Arrays.asList(3f, 2f, 0f),
                Arrays.asList(2f, 3f, 0f)));

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0,  builder.countClassInGroup(3f, group));
    }

    @Test
    void countClassInGroupTest2() {
        Group group = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(2f, 1f, 1f),
                Arrays.asList(3f, 2f, 0f),
                Arrays.asList(2f, 3f, 0f)));

        TreeBuilder builder = new TreeBuilder();

        assertEquals(2,  builder.countClassInGroup(1f, group));
    }

    @Test
    void countClassesTest1 () {

        Group group = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(2f, 1f, 1f),
                Arrays.asList(3f, 2f, 0f),
                Arrays.asList(2f, 3f, 0f)));

        TreeBuilder builder = new TreeBuilder();
        List<Float> classes = Arrays.asList(1f, 0f);

        assertEquals(classes,  builder.countClasses(group));
    }

    @Test
    void countClassesTest2 () {

        Group group = new Group();

        TreeBuilder builder = new TreeBuilder();

        List<Float> classes = new ArrayList<>();

        assertEquals(classes,  builder.countClasses(group));

    }

    @Test
    void countClassesTest3 () {

        Group group = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(2f, 1f, 1f)));

        TreeBuilder builder = new TreeBuilder();
        List<Float> classes = Arrays.asList(1f);

        assertEquals(classes,  builder.countClasses(group));
    }

    @Test
    void findBestClassInGroupTest1() {

        Group group = new Group(Arrays.asList(
                Arrays.asList(1f, 4f, 1f),
                Arrays.asList(3f, 7f, 0f),
                Arrays.asList(2f, 1f, 1f),
                Arrays.asList(3f, 2f, 0f),
                Arrays.asList(2f, 3f, 0f)));

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0f,  builder.findBestClassInGroup(group));
    }
}
